window.onload = function() {
    const ratio = .1;
    const options = {
        root: null,
        rootMargin: '0px',
        threshold: .5
      }
      const handleIntersect = function (entries, observer) {
          entries.forEach(function (entry) {
              if(entry.intersectionRatio > ratio){
                  entry.target.classList.add('reveal-visible')
                  console.log('visible')
                  observer.unobserve(entry.target) 
              } 
              
              
          } )
      }
      
      const observer = new IntersectionObserver(handleIntersect, options);
      document.querySelectorAll('[class*="reveal-"]').forEach(function(reveal){
        observer.observe(reveal)
      })



}
