<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\MessageType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    /**
     * @Route("/Contact", name="Contact")
     */
    public function Contact(Request $request,\Swift_Mailer  $mailer): Response
    {
        $message = new Message;
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {

            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($message);
            $doctrine->flush();

            $contact = $form->getData();
            $message = (new \Swift_Message('Nouveau Contact'))
                ->setFrom('noorhumanitiy@gmail.com')
                ->setTo('benamaramosab@yahoo.fr')
                ->setBody(
                    $this->renderView('emails/contact.html.twig', compact('contact'),'text/html')
                )
                ;
                $mailer->send($message);

            $this->addFlash('success', 'Le message a bien ete envoyer');
        }
        return $this->render('contact/index.html.twig', [
            'form' => $form->createView(),
        ]);

    }
}
