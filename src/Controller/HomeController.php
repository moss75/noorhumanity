<?php

namespace App\Controller;

use App\Entity\Message;
use App\Form\MessageType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        return $this->render('home/index.html.twig', [
            
        ]);
    }
    /**
     * @Route("/QuiSommeNous", name="QuiSommeNous")
     */
    public function WeAre()
    {
        return $this->render('home/WeAre.html.twig', [
            
        ]);
    }
    /**
     * @Route("/Action", name="Acion")
     */
    public function Acion()
    {
        return $this->render('home/Action.html.twig', [
            
        ]);
    }
}
