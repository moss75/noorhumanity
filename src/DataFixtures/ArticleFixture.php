<?php

namespace App\DataFixtures;

use App\Entity\Articles;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker;
use Mmo\Faker\PicsumProvider;
use Symfony\Component\HttpFoundation\File\File;

class ArticleFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
     $faker = Faker\Factory::create('fr_FR');
     $faker->addProvider(new PicsumProvider($faker));

        for ($i = 0; $i < 10; $i++){
            $image = $faker->picsum('./public/images', random_int(1152, 2312), random_int(865,1736));
            $article = new Articles;
            $article->setTitre($faker->word);
            $article->setCreatedAt($faker->dateTimeBetween('-4 years'));
            $article->setContenu($faker->paragraphs($nb = 3, $asText = true));
            $article->setImageFile(new File($image));
            $article->setImages(str_replace('./public/images\\', '', $image));

            $manager->persist($article);
        }

        $manager->flush();
    }
}
